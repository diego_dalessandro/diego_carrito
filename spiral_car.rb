require "colorize"


def main()
    #Inicializacion de variables
    board_rows = 10
    board_cols = 10
    board = []
    center = []
    current_position = []
    path = []
    cell_length = 3
    iteration = 0
    exit = false
    set_starting_position(board_rows, board_cols, current_position, center)
    
    loop do
        system("clear")
        clear_board(board_rows, board_cols, board, cell_length)
        drawn_current_position(board, current_position, cell_length)
        drawn_path(board, path, cell_length)
        drawn_board(board)
        drawn_info(board, current_position, path, iteration)

        if !choose_direction(board, center, current_position, path)
            exit = true
        end
        iteration += 1
        sleep(0.2)

        #gets
        break if exit
    end

end

def draw_cell(cell_length)
    cell = ""
    for i in 0...cell_length
        cell += " "
    end
    return cell
end

def clear_board(board_rows, board_cols, board, cell_length)
    for row in 0...board_rows
        new_row = []
        for col in 0...board_cols
            new_row.push draw_cell(cell_length).colorize(:background => :white)
        end
        board[row] = new_row
    end
end

def drawn_current_position(board, current_position, cell_length)
    for row in 0...board.length
        for col in 0...board[row].length
            if row == current_position[0] && col == current_position[1]
                board[row][col] = draw_cell(cell_length).colorize(:background => :red)
            end
        end
    end
end

def drawn_path(board, path, cell_length)
    for i in 0...path.length
        for row in 0...board.length
            for col in 0...board[row].length
                if row == path[i][0] && col == path[i][1]
                    board[row][col] = draw_cell(cell_length).colorize(:background => :yellow)
                end
            end
        end
    end
end

def drawn_board(board)
    puts "Tablero #{board.length} x #{board[0].length}"
    for row in 0...board.length
        for col in 0...board[0].length
            print board[row][col]
        end
        puts
    end
    puts
end

def drawn_info(board, current_position, path, iteration)
    puts "Posicion actual del carro: #{current_position[0]}, #{current_position[1]} "
    puts "Ciclo no: #{iteration + 1}"
end

def choose_direction(board, center, current_position, path)
    up = [-1, 0]
    down = [1, 0]
    left = [0, -1]
    right = [0, 1]

    if (current_position[0] >= center[0])
        if (current_position[1] <= center[1])
            if is_available(board, current_position, up, path)
                move(current_position, up, path)
            elsif is_available(board, current_position, left, path)
                move(current_position, left, path)
            elsif is_available(board, current_position, down, path)
                move(current_position, down, path)
            elsif is_available(board, current_position, right, path)
                move(current_position, right, path)
            else 
                return false
            end
        else
            if is_available(board, current_position, left, path)
                move(current_position, left, path)
            elsif is_available(board, current_position, down, path)
                move(current_position, down, path)
            elsif is_available(board, current_position, right, path)
                move(current_position, right, path)
            elsif is_available(board, current_position, up, path)
                move(current_position, up, path)    
            else
                return false
            end
        end
    else
        if (current_position[1] <= center[1])
            if is_available(board, current_position, right, path)
                move(current_position, right, path)
            elsif is_available(board, current_position, up, path)
                move(current_position, up, path)
            elsif is_available(board, current_position, left, path)
                move(current_position, left, path)
            elsif is_available(board, current_position, down, path)
                move(current_position, down, path)
            else
                return false
            end
        else
            if is_available(board, current_position, down, path)
                move(current_position, down, path)
            elsif is_available(board, current_position, right, path)
                move(current_position, right, path)
            elsif is_available(board, current_position, up, path)
                move(current_position, up, path)
            elsif is_available(board, current_position, left, path)
                move(current_position, left, path)
            else
                return false
            end
        end
    end
    return true
end

def is_available(board, current_position, vector, path)
    possible_position = []
    possible_position[0] = current_position[0] + vector[0]
    possible_position[1] = current_position[1] + vector[1]

    return false if possible_position[0] >= board.length || possible_position[0] < 0
    return false if possible_position[1] >= board[0].length || possible_position[1] < 0
    for i in 0...path.length
        if possible_position[0] == path[i][0] && possible_position[1] == path[i][1]
            return false
        end
    end
    return true
end

def move(current_position, vector, path)
    new_path = [current_position[0], current_position[1]]
    path.push new_path
    current_position[0] += vector[0]
    current_position[1] += vector[1]
end

def set_starting_position(board_rows, board_cols, current_position, center)
    board_rows -= 1 if board_rows % 2 == 0
    board_cols -= 1 if board_cols % 2 == 0
    
    row0 = board_rows/2
    col0 = board_cols/2

    current_position[0] = row0
    current_position[1] = col0

    center[0] = row0
    center[1] = col0
end




main
print "Presiona \"Enter\" tecla para terminar"
gets
system("clear")